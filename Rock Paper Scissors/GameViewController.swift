//
//  GameViewController.swift
//  Rock Paper Scissors
//
//  Created by Miguel Gil Aleixandre on 19/5/17.
//  Copyright © 2017 Miguel Gil Aleixandre. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cont = segue.destination as! ResultViewController
        if segue.identifier == "rock" {
            cont.userChoice = 0
        }
        if segue.identifier == "paper" {
            cont.userChoice = 1
        }
        
    }
    
    @IBAction func paperClick(){
        performSegue(withIdentifier: "paper", sender: self)
    }
    
    @IBAction func scissorsClick(){
        let resultController =  self.storyboard?.instantiateViewController(withIdentifier: "ResultController") as!ResultViewController
        resultController.userChoice = 2
        present(resultController, animated: true, completion: nil)
    }

}

