//
//  ResultViewController.swift
//  Rock Paper Scissors
//
//  Created by Miguel Gil Aleixandre on 19/5/17.
//  Copyright © 2017 Miguel Gil Aleixandre. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    var userChoice: Int!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var message: UILabel!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let userChoice = self.userChoice {
            let randomValue = Int(arc4random() % 3)
            switch userChoice {
            case 0:
                rockChoice(randomValue: randomValue)
            case 1:
                paperChoice(randomValue: randomValue)
            case 2:
                scissorsChoice(randomValue: randomValue)
            default:
                break
            }
        }
    }
    
    func rockChoice(randomValue: Int){
        switch randomValue {
        case 0:
            self.image.image = #imageLiteral(resourceName: "itsATie")
            message.text = "Rock vs Rock\nTie"
        case 1:
            self.image.image = #imageLiteral(resourceName: "PaperCoversRock")
            message.text = "Rock vs Paper\nYou Lose"
        case 2:
            self.image.image = #imageLiteral(resourceName: "RockCrushesScissors")
            message.text = "Rock vs Scissors\nYou Win!"
        default:
            break
        }
    }
    
    func paperChoice(randomValue: Int){
        switch randomValue {
        case 0:
            self.image.image = #imageLiteral(resourceName: "PaperCoversRock")
            message.text = "Paper vs Rock\nYou Win!"
        case 1:
            self.image.image = #imageLiteral(resourceName: "itsATie")
            message.text = "Paper vs Paper\nTie"
        case 2:
            self.image.image = #imageLiteral(resourceName: "ScissorsCutPaper")
            message.text = "Paper vs Scissors\nYou Lose"
        default:
            break
        }
    }
    
    func scissorsChoice(randomValue: Int){
        switch randomValue {
        case 0:
            self.image.image = #imageLiteral(resourceName: "RockCrushesScissors")
            message.text = "Scissors vs Rock\nYou Lose"
        case 1:
            self.image.image = #imageLiteral(resourceName: "ScissorsCutPaper")
            message.text = "Scissors vs Paper\nYou Win!"
        case 2:
            self.image.image = #imageLiteral(resourceName: "itsATie")
            message.text = "Scissors vs Scissors\nTie"
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
